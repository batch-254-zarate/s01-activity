package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    // Spring Boot s02 Activity
    @Id

    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    // To avoid infinite recursion, use the @JsonIgnore annotation.
    @JsonIgnore
    private Set<Post> posts;

    public Set<Post> getPosts() {
        return posts;
    }


    // default constructor
    public User() {
    }

    // parameterized constructor
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    // Getter & Setter
    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
