package com.zuitt.wdc044.models;

import javax.persistence.*;

// mark this Java object as a representation of database table via @Entity
@Entity
// designate table name via @Table annotation.
@Table(name = "posts")
public class Post {

    // indicate that this property represents the primary key via @Id
    @Id

    // values for this property will be auto-incremented - has only one system in numbering objects
    @GeneratedValue
    private Long id;

    // class properties that represent table columns in a rational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;


    // default constructor - this is needed when retrieving posts
    public Post(){}

    // parameterized constructor
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }


    public User getUser() {
        return user;
    }

    public void setUser (User user) {
        this.user = user;
    }

    // Getter & Setter
    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
